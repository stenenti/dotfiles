(define-package "modus-themes" "20220920.539" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "10a0c9f4220257850e17208626d6d3fef611d6b5" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
