;; NOTE: init.el is now generated from Emacs.org.  Please edit that file
;;       in Emacs and init.el will be generated automatically!

; define some graphical variables ; questo modo alternativo di definire e richiamare le variabili può essere comodo ma non necessariamente più facile da capire all'inizio.
;(defvar efs/default-face FiraCode Nerd Font)  
;(defvar efs/fixed-pitch-face Mononoki)
;(defvar efs/default-face FiraCode Nerd Font)
(defvar efs/default-font-size 180) ; adjust this font size for your system. perchè la variabile si chiama efs/nome-variabile?
(defvar efs/default-variable-font-size 180) ; non dovrebbe essere espressa in % rispetto al default pitch? per esempio 1.0 invece di 180 vedi [[https://protesilaos.com/codelog/2020-09-05-emacs-note-mixed-font-heights/]]
(defvar efs/frame-transparency '(85 . 85)) ; Make frame transparency overridable

;; Package sytem setup
;; Emacs has a built in package manager but it doesn’t make it easy to automatically install packages on a new system the first time you pull down your configuration. use-package is a really helpful package used in this configuration to make it a lot easier to automate the installation and configuration of everything else we use.

;; Initialize package sources
; [[https://jeffkreeftmeijer.com/emacs-straight-use-package/]] interessante sintesi sui package managers

(require 'package)
; package è un package manager per emacs, in alternativa ce ne sono altri, tipo straight.el
; Straight.el is an alternative package manager that installs packages through Git checkouts instead of downloading tarballs from one of the package archives. Doing so allows installing forked packages, altering local package checkouts, and locking packages to exact versions for reproducable setups. per ora uso solo package, straight lo tengo per un momento futuro.
; [[https://wikemacs.org/wiki/Package.el]]

(setq package-archives '(("melpa" . "https://melpa.org/packages/") ; MELPA è il repository degli utenti Emacs, tipo AUR
                         ("org" . "https://orgmode.org/elpa/") ; repository del pacchetto org
                         ("elpa" . "https://elpa.gnu.org/packages/"))) ; ELPA è il repository ufficiale dei pachetti Emacs.
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

 ;; Initialize use-package on non-Linux platforms, da rivedere quando servirà, per ora lasciamo stare
;(unless (package-installed-p 'use-package)
;  (package-install 'use-package))
					;

(require 'use-package) ; [[https://jwiegley.github.io/use-package/]] 
(setq use-package-always-ensure t) ; cosa fa questo?


;; Basic UI configuration

(setq inhibit-startup-message t) ; disable startup message
(scroll-bar-mode -1) ; disable visible scrollbar
(tool-bar-mode -1) ; disable toolbar
(tooltip-mode 1) ; disable tooltip (-1)
(set-fringe-mode 10) ; give some breathing room
(menu-bar-mode 1) ; disable menubar (-1), per ora la lascio
(setq visible-bell t) ;setup the visible bell
(column-number-mode)
(global-display-line-numbers-mode t)
(add-hook 'text-mode-hook 'visual-line-mode) ; va a capo in modo "soft" ossia senza inserire il simbolo "a capo" nella linea se la linea supera il bordo dx della window. a questo punto i comandi next/previous line si adattano alle righe visuali (e non logiche). per attivare/disattivare il ritorno a capo all'interno di una parola, ~toggle M-x visual-line-mode~
;(visual-line-mode t) ; questo va assieme al parametro di sopra ma non fa quello che si si aspetta, ossia andare a capo senza pezzare la parola. da rivedere
(setq-default cursor-type 'bar) ; set cursor like a bar instead of a box (default value)
(set-cursor-color "#ffffff") ; ffffff is white 
(load-theme 'tango-dark 1) ; set theme. altri temi alternativi: modus (con colori molto contrastati)
(set-frame-parameter (selected-frame) 'alpha efs/frame-transparency) ;; Set frame transparency (frame [ la window nel linguaggio emacs) al valore indicato nella variabile creata all'inizio del file.
(add-to-list 'default-frame-alist `(alpha . ,efs/frame-transparency))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

; da aggiungere: come togliere/pulire i folder dai file temp che finiscono con ~ ? es. .org~ opure .el~. è possibile archiviarlitutti inun folder dedicato chje viene p0oi svuotato alla fine ?


;; Font configuration
(set-face-attribute 'default nil :font "FiraCode Nerd Font" :height efs/default-font-size) ; set the default face. la taglia del font è definita dalla variabile creata all‘inizio del file.
(set-face-attribute 'fixed-pitch nil :font "Mononoki" :height efs/default-font-size) ; Set the fixed pitch face
(set-face-attribute 'variable-pitch nil :font "Ubuntu" :height efs/default-variable-font-size :weight 'regular) ; Set the variable pitch face

;; Org mode

(use-package org) ; viene solo installato, per ora non ci sono personalizzazioni.


;; Org-Roam

(use-package org-roam) ; [[https://www.orgroam.com/manual.html]]

(make-directory "~/org-roam")
(setq org-roam-directory (file-truename "~/org-roam")) ; [[https://www.orgroam.com/manual.html#Setting-up-Org_002droam]] ~/org-roam è la dir di default dove viene messo tutto org-roam. si può inserire un path diverso
; Next, we setup Org-roam to run functions on file changes to maintain cache consistency. This is achieved by running M-x org-roam-db-autosync-mode. To ensure that Org-roam is available on startup, place this in your Emacs configuration. The file-truename function is only necessary when you use symbolic links inside org-roam-directory: Org-roam does not resolve symbolic links. One can however instruct Emacs to always resolve symlinks, at a performance cost:
(setq find-file-visit-truename t)

(org-roam-db-autosync-mode) ; per il database che tiene tutto sotto controllo tenendo tutti i link e i nodi in una cache
; To build the cache manually, run
; ~M-x org-roam-db-sync~
; Cache builds may take a while the first time, but subsequent builds are often instantaneous because they only reprocess modified files. 





;; Org Roam ; la parte qui sotto è presa da [[https://config.daviwil.com/emacs]]
;(use-package org-roam
;  :straight t ; usa straight.el come package manager, assieme a packages.el>
;  :hook
;  (after-init . org-roam-mode)
;  :custom
;  (org-roam-directory "~/Notes/Roam/")
;  (org-roam-completion-everywhere t)
;  (org-roam-completion-system 'default)
;  (org-roam-capture-templates
;    '(("d" "default" plain
;       #'org-roam-capture--get-point
;       "%?"
;       :file-name "%<%Y%m%d%H%M%S>-${slug}"
;       :head "#+title: ${title}\n"
;       :unnarrowed t)
;      ("ll" "link note" plain
;       #'org-roam-capture--get-point
;       "* %^{Link}"
;       :file-name "Inbox"
;       :olp ("Links")
;       :unnarrowed t
;       :immediate-finish)
;      ("lt" "link task" entry
;       #'org-roam-capture--get-point
;       "* TODO %^{Link}"
;       :file-name "Inbox"
;       :olp ("Tasks")
;       :unnarrowed t
;       :immediate-finish)))
;  (org-roam-dailies-directory "Journal/")
;  (org-roam-dailies-capture-templates
;    '(("d" "default" entry
;       #'org-roam-capture--get-point
;       "* %?"
;       :file-name "Journal/%<%Y-%m-%d>"
;       :head "#+title: %<%Y-%m-%d ;%a>\n\n[[roam:%<%Y-%B>]]\n\n")
;      ("t" "Task" entry
;       #'org-roam-capture--get-point
;       "* TODO %?\n  %U\n  %a\n  %i"
;       :file-name "Journal/%<%Y-%m-%d>"
;       :olp ("Tasks")
;       :empty-lines 1
;       :head "#+title: %<%Y-%m-%d ;%a>\n\n[[roam:%<%Y-%B>]]\n\n")
;      ("j" "journal" entry
;       #'org-roam-capture--get-point
;       "* %<%I:%M %p> - Journal  :journal:\n\n%?\n\n"
;       :file-name "Journal/%<%Y-%m-%d>"
;       :olp ("Log")
;       :head "#+title: %<%Y-%m-%d ;%a>\n\n[[roam:%<%Y-%B>]]\n\n")
;      ("l" "log entry" entry
;       #'org-roam-capture--get-point
;       "* %<%I:%M %p> - %?"
;       :file-name "Journal/%<%Y-%m-%d>"
;       :olp ("Log")
;       :head "#+title: %<%Y-%m-%d ;%a>\n\n[[roam:%<%Y-%B>]]\n\n")
;      ("m" "meeting" entry
;       #'org-roam-capture--get-point
;       "* %<%I:%M %p> - %^{Meeting Title}  ;:meetings:\n\n%?\n\n"
;       :file-name "Journal/%<%Y-%m-%d>"
;       :olp ("Log")
;       :head "#+title: %<%Y-%m-%d ;%a>\n\n[[roam:%<%Y-%B>]]\n\n")))
;  :bind (:map org-roam-mode-map
;          (("C-c n l"   . org-roam)
;           ("C-c n f"   . org-roam-find-file)
;           ("C-c n d"   . org-roam-dailies-find-date)
;           ;("C-c n c"   . ;org-roam-dailies-capture-today)
;           ;("C-c n C r" . ;org-roam-dailies-capture-tomorrow)
;           ("C-c n t"   . ;org-roam-dailies-find-today)
;           ;("C-c n y"   . ;org-roam-dailies-find-yesterday)
;           ;("C-c n r"   . ;org-roam-dailies-find-tomorrow)
;           ("C-c n g"   . org-roam-graph))
;         :map org-mode-map
;         (("C-c n i" . org-roam-insert))
;         (("C-c n I" . org-roam-insert-immediate))))


					; la parte che segue può essere spostata in un file separato per non appesantrire il file init.el. oppure può essere soppressa del tutto mdificando solo init.el in maniera diversa? da approfondire [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Customizations.html]]
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(org-roam-timestamps org-roam-bibtex org-roam-ui use-package org-roam modus-themes)))
; forse gestendo le instzallazioni con un altro package manageer, tipo straight.el, questa parte diventa inutile. o forse può essere spostata in alto dove si gestisce packageg.el [[https://emacs.stackexchange.com/questions/59084/package-selected-packages-what-is-it-useful-for]]
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
; esiste un'altra sezione di font configuration più in alto. come interagiscono le due? forse questa è meglio toglierla, da verificare, tanto per ora è vuota.
