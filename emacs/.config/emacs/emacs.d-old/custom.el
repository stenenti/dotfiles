(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("dbec134fb889afe6034edc2dcd881dbfa302eed9823383f795b5c00517e0ea17" "7451f243a18b4b37cabfec57facc01bd1fe28b00e101e488c61e1eed913d9db9" default))
 '(package-selected-packages
   '(sqlite3 org-roam lua-mode all-the-icons-ivy all-the-icons-dired all-the-icons-ibuffer-mode all-the-icons-ivy-rich-mode major-mode-icons all-the-icons-ibuffer all-the-icons-ivy-rich magit haskell-mode modus-vivendi-theme which-key use-package try modus-operandi-theme image-dired+ dracula-theme doom-modeline counsel command-log-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
