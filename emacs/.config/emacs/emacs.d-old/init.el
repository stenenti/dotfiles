

;;;; General

(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room
(menu-bar-mode -1)          ; Disable the menu bar

;; check which OS is in use
(pcase system-type
  ('gnu/linux "Linux")
  ('windows-nt "Windows")
  ('darwin "MacOS"))

;; check if config is being loaded in the daemon
(if (daemonp)
    (message "Loading in the daemon")
    (message "Loading in regular Emacs"))

;; Graphical settings
;; fonts: define the function efs/set-font-faces that will be loaded in emacsclient AFTER the daemon has started
(defun efs/set-font-faces ()
  (message "Setting faces!")
  (set-face-attribute 'default nil :font "UbuntuMono" :height 200)

  ;; set the fixed pitch face
  (set-face-attribute 'fixed-pitch nil :font "mononoki" :height 180)

  ;;set the variable pitch face
  (set-face-attribute 'variable-pitch nil :font "mononoki" :height 180 :weight 'regular))

(if (daemonp)
    (add-hook 'after-make-frame-functions
	      (lambda (frame)
 		(setq doom-modeline-icon t)
	        (with-selected-frame frame
		  (efs/set-font-faces))))
    (efs/set-font-faces))





;; linux
;;(add-hook 'after-make-frame-functions
;;	  (lambda (f) (set-face-attribute 'default nil
;;		    :font "mononoki"
;;		    :height 180)))

;;(add-hook 'text-mode-hook
;;	  'lambda() (turn-on-auto-fill)
;;	  (set-fill-column 80))


;;on windows 10
;;(set-face-attribute 'default nil :font "mononoki" :height 180)

;; setup highlight line mode
(global-hl-line-mode 1)

;; theme 
(load-theme 'dracula t) ; per caricarne altri: "M-x load-theme"

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; set custon.el to store custom-set-vatiables
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)


 ;;;; Packages

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
 (unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package command-log-mode)

;; ivy mode per ricerche
(use-package ivy
  :bind (("C-s" . swiper)
	 ("M-x" . counsel-M-x)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)	
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t))


;; doom-modeline per la barra supra il minibuffer
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))



;; -*- mode: elisp -*-

;; Disable the splash screen (to enable it again, replace the t with 0)
;; (setq inhibit-splash-screen 0)

;; Enable transient mark mode
;;(transient-mark-mode 1)

;;;;Org mode configuration

;; Enable Org mode and Org-roam
(require 'org)
;; Make Org mode work with files ending in .org
;; (add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
;; The above is the default in recent emacsen

(setq org-hide-leading-stars t)

;;; Tell Emacs where sqlite3.exe is stored
(add-to-list 'exec-path "/usr/bin/sqlite3")

;;; Tell Emacs to start org-roam-mode when Emacs starts
(add-hook 'after-init-hook 'org-roam-mode)
(setq org-roam-directory "~/org-roam")



;;; Define key bindings for Org-roam
(global-set-key (kbd "C-c n r") #'org-roam-buffer-toggle-display)
(global-set-key (kbd "C-c n i") #'org-roam-insert)
(global-set-key (kbd "C-c n /") #'org-roam-find-file)
(global-set-key (kbd "C-c n b") #'org-roam-switch-to-buffer)
(global-set-key (kbd "C-c n d") #'org-roam-find-directory)

;;; Let's also assign C-z to undo here
(global-set-key (kbd "C-z") 'undo) ;Emacs defaultog roma db is bound to hide Emacs.






;;(setq org-todo-keywords
;;      '((sequence "TODO" "IN-PROGRESS" | "DONE" "CANCELLED")))

;;(global-set-key (kbd "C-c l") 'org-store-link)
;;(global-set-key (kbd "C-c a") 'org-agenda)
;;(global-set-key (kbd "C-c c") 'org-capture)


;;;; Email
;; start mu4e
;;(add-to-list 'load-path /usr/share/emacs/site-lisp/mu4e)
;;(require '/usr/share/emacs/site-lisp/mu4e/mu4e)
;;(set;; mu4e-sent-folder     "purogelatoSten/Sent"
;; mu4e-drafts-folder   "purogelatoSten/Drafts"
;; mu4e-trash-folder    "purogelatoSten/Trash"
;; mu4e-refile-folder   "purogelatoSten/Archive")
;;(setq
;;  mu4e-get-mail-command "offlineimap"   ;; or fetchmail, or ...
;;  mu4e-update-interval 200)             ;; update every X seconds
