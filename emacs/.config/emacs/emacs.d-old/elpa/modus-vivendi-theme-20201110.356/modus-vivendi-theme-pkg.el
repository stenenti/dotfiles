(define-package "modus-vivendi-theme" "20201110.356" "Accessible dark theme (WCAG AAA)"
  '((emacs "26.1"))
  :commit "019517e6ea892131170c1d5aa57bbc4e5b4650d2" :keywords
  ("faces" "theme" "accessibility")
  :authors
  (("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainer
  ("Protesilaos Stavrou" . "info@protesilaos.com")
  :url "https://gitlab.com/protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
