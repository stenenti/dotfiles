-------------------------------------------------------------------------
-- IMPORTS
-------------------------------------------------------------------------
  -- Base
import XMonad
import Data.Monoid
import qualified Data.Map as M
import Data.Tree
import System.IO
import System.Exit
import qualified XMonad.StackSet as W

  -- Actions
import XMonad.Actions.GridSelect
import XMonad.Actions.SpawnOn (spawnOn, manageSpawn) -- to spawn application on specific windows
import qualified XMonad.Actions.TreeSelect as TS

  -- Config

  
  -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers 
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

  --  Layout
import XMonad.Layout.IndependentScreens
import XMonad.Layout.Spacing

  -- Prompt

  
  -- Utilities
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig

-------------------------------------------------------------------------
-- GENERAL SETTINGS
-------------------------------------------------------------------------
  -- Variables
myFont               :: String
myFont               = "xft:Mononoki Nerd Font:bold:size=9:antialias=true:hinting=true"

myTerminal           :: String
myTerminal           = "alacritty"

myBrowser            :: String
myBrowser            = "firefox " -- Sets browser for tree select

myModMask            :: KeyMask
myModMask            = mod4Mask     -- Sets modkey to super/windows key

myBorderWidth        :: Dimension
myBorderWidth        = 6 -- Width of the window border in pixels

myNormalBorderColor  :: String
myNormalBorderColor  = "#000000" --black

myFocusedBorderColor :: String
myFocusedBorderColor = "#FF6300" --"#00BBFF" --"#FF6300" "#900C3F"

myFocusFollowsMouse  :: Bool
myFocusFollowsMouse  = False -- Whether focus follows the mouse pointer.

myClickJustFocuses   :: Bool
myClickJustFocuses   = True -- Whether clicking on a window to focus also passes the click to the window

myWorkspaces         = ["1-FM","2-MAIN","3-WEB","4-MAIL","5-LO","6-PHOTO","7","8","9-SYS"]

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

  -- Autostart
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "lxsession &"
  spawnOnce "xfce4-power-manager &"
  spawnOnce "/usr/bin/stalonetray &"
  spawnOnce "picom --config /home/sten/.config/picom/picom.conf &"
  spawnOnce "synology-drive autostart &"
  spawnOnce "/opt/protonmail-bridge/proton-bridge --no-window &"
  spawnOnce "feh --bg-scale ~/Pictures/Wallpapers/arch02.jpg &"
  spawnOnce "flameshot &"
  spawnOnce "nm-applet &"
  spawnOnce "blueman-applet &"
--  spawnOnce "pulseaudio -D &"
  spawnOnce "volumeicon &"
  spawnOnce "pa-applet &"
  spawnOnce "/usr/bin/emacs --daemon &"
  setWMName "LG3D" -- May be useful for making Java GUI programs work

-------------------------------------------------------------------------
-- KEY BINDINGS
-------------------------------------------------------------------------

myKeys =
    [
  -- firefox and specific sites
      ("M-x f", spawn "firefox")
--    ("M-x fr", spawn "firefox")
    , ("M-x v", spawn "Evolution")
    , ("M-x r", spawn "ranger")
    , ("M-x t", spawn "Thunar")

  -- emacs (Super-x followed by a key)
    , ("M-x e", spawn "emacsclient -c -a 'emacs'")                                      -- start emacs
    , ("M-x x", spawn "emacsclient -c -a 'emacs' ~/.dotfiles/xmonad/.xmonad/xmonad.hs") -- emacs + xmonad.hs
    , ("M-x d", spawn "emacsclient -c -a 'emacs' ~/")                                   -- emacs + dired

  -- TreSelect
    , ("C-t t", treeselectAction tsDefaultConfig)
    ] ++
      -- changes workspace switching bindings to use "view" rather than "greedyView"
    [ (otherModMasks ++ "M-" ++ [key], action tag)
      | (tag, key)  <- zip myWorkspaces "123456789"
      , (otherModMasks, action) <- [ ("", windows . W.view) -- was W.greedyView
                                   , ("S-", windows . W.shift)]
    ]
    

{-
Le seul raccourci qu’il me manquait afin que ce soit parfait, était le « mod + flèche » afin de se déplacer sur le workspace de droite ou de gauche. Pour ce faire, il suffit de rajouter les lignes suivantes dans le tableau des raccourcis personnalisés :

, ((modMask, xK_Right), nextWS)
, ((modMask .|. shiftMask, xK_Right), shiftToNext)
, ((modMask, xK_Left), prevWS)
, ((modMask .|. shiftMask, xK_Left), shiftToPrev)
-}


-------------------------------------------------------------------------
-- MOUSE BINDINGS
-------------------------------------------------------------------------


------------------------------------------------------------------------
-- HOOKS, LAYOUTS
-------------------------------------------------------------------------

  -- LogHook
myLogHook = ()

  -- ManageHook
  -- to get the className of an application, run the following command in a terminal
  -- $ xprop | grep WM_CLASS
  -- and click with the mouse the window with the running application
  -- the terminal will return " WM_CLASS(STRING) = "Navigator", "firefox" " (in the case of firefox)
  -- className is the second element in WM_CLASS(STRING), that is "firefox" in the case above
myManageHook = composeAll
  [ className =? "Thunar"                  --> doShift "1-FM"
  , className =? "dolphin"                 --> doShift "1-FM"
  , className =? "ranger"                  --> doShift "1-FM"
  , className =? "Emacs"                   --> doShift "2-EMACS"
  , className =? "firefox"                 --> doShift "3-WEB"
  , className =? "Evolution"               --> doShift "4-MAIL"
  , className =? "libreoffice-calc"        --> doShift "5-LO"
  , className =? "libreoffice-writer"      --> doShift "5-LO"
  , className =? "libreoffice-startcenter" --> doShift "5-LO"
  , className =? "digikam"                 --> doShift "6-PHOTO"
  , className =? "Darktable"               --> doShift "6-PHOTO"
  , className =? "cloud-drive-ui"          --> doFloat
  , className =? "ProtonMail Bridge"       --> doFloat
  , manageDocks
  ]
  
  -- Layouts

mySpacingRaw = spacingRaw
  False
  (Border 0 10 10 10)
  True
  (Border 10 10 10 10)
  True
  
myLayoutHook = mySpacingRaw $ avoidStruts $ tiled ||| Mirror tiled ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio
     -- The default number of windows in the master pane
     nmaster = 1
     -- Default proportion of screen occupied by master pane
     ratio   = 2/3
     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

-------------------------------------------------------------------------
-- TREESELECT
-------------------------------------------------------------------------

{-
TreeSelect displays your workspaces or actions in a Tree-like format.  You can select desired workspace/action with the cursor or hjkl keys.
-}

  -- TreeSelect menu entries

treeselectAction :: TS.TSConfig (X ()) -> X ()
treeselectAction a = TS.treeselectAction a
   [ Node (TS.TSNode "+ dotfiles" "system settings" (return ()))
       [ Node (TS.TSNode "dotfiles" "xmonad setting all dotfiles" (spawn "emacsclient -c -a 'emacs' ~/.dotfiles")) []
       , Node (TS.TSNode "emacs init.el" "emacs settings" (spawn "emacsclient -c -a 'emacs' ~/.dotfiles/emacs/.emacs.d/init.el")) []
       , Node (TS.TSNode "xmonad.hs" "xmonad settings" (spawn "emacsclient -c -a 'emacs' ~/.dotfiles/xmonad/.xmonad/xmonad.hs")) []
       ]
   , Node (TS.TSNode "+ Office" "office applications" (return ()))
       [ Node (TS.TSNode "LibreOffice" "Open source office suite" (spawn "libreoffice")) []
       , Node (TS.TSNode "LibreOffice Base" "Desktop database front end" (spawn "lobase")) []
       , Node (TS.TSNode "LibreOffice Calc" "Spreadsheet program" (spawn "localc")) []
       , Node (TS.TSNode "LibreOffice Draw" "Diagrams and sketches" (spawn "lodraw")) []
       , Node (TS.TSNode "LibreOffice Impress" "Presentation program" (spawn "loimpress")) []
       , Node (TS.TSNode "LibreOffice Math" "Formula editor" (spawn "lomath")) []
       , Node (TS.TSNode "LibreOffice Writer" "Word processor" (spawn "lowriter")) []
       , Node (TS.TSNode "Zathura" "PDF Viewer" (spawn "zathura")) []
       ]
   , Node (TS.TSNode "+ Search and Reference" "Search engines, indices and wikis" (return ()))
       [ Node (TS.TSNode "DuckDuckGo" "Privacy-oriented search engine" (spawn (myBrowser ++ "https://duckduckgo.com/"))) []
       , Node (TS.TSNode "Google" "The evil search engine" (spawn (myBrowser ++ "http://www.google.com"))) []
       , Node (TS.TSNode "Thesaurus" "Lookup synonyms and antonyms" (spawn (myBrowser ++ "https://www.thesaurus.com/"))) []
       , Node (TS.TSNode "Wikipedia" "The free encyclopedia" (spawn (myBrowser ++ "https://www.wikipedia.org/"))) []
       ]
   , Node (TS.TSNode "+ Haskell" "haskell documentation" (return ()))
       [ Node (TS.TSNode "Haskell.org" "Homepage for haskell" (spawn (myBrowser ++ "http://www.haskell.org"))) []
       , Node (TS.TSNode "Hoogle" "Haskell API search engine" (spawn "https://hoogle.haskell.org/")) []
       , Node (TS.TSNode "r/haskell" "Subreddit for haskell" (spawn (myBrowser ++ "https://www.reddit.com/r/haskell/"))) []
       , Node (TS.TSNode "Haskell on StackExchange" "Newest haskell topics on StackExchange" (spawn (myBrowser ++ "https://stackoverflow.com/questions/tagged/haskell"))) []
       ]
   ]

  -- TreeSelect settings
{- Configuration options for the treeSelect menus.  Keybindings for treeSelect menus. Use h-j-k-l to navigate.  Use ‘o’ and ‘i’ to move forward/back in the workspace history.  Single KEY’s are for top-level nodes. SUPER+KEY are for the second-level nodes. SUPER+ALT+KEY are third-level nodes. -}


tsDefaultConfig :: TS.TSConfig a
tsDefaultConfig = TS.TSConfig { TS.ts_hidechildren = True
                              , TS.ts_background   = 0xdd282c34
                              , TS.ts_font         = myFont
                              , TS.ts_node         = (0xffd0d0d0, 0xff1c1f24)
                              , TS.ts_nodealt      = (0xffd0d0d0, 0xff282c34)
                              , TS.ts_highlight    = (0xffffffff, 0xff755999)
                              , TS.ts_extra        = 0xffd0d0d0
                              , TS.ts_node_width   = 200
                              , TS.ts_node_height  = 20
                              , TS.ts_originX      = 100
                              , TS.ts_originY      = 100
                              , TS.ts_indent       = 80
                              , TS.ts_navigate     = myTreeNavigation
                              }

myTreeNavigation = M.fromList
    [ ((0, xK_Escape),   TS.cancel)
    , ((0, xK_Return),   TS.select)
    , ((0, xK_space),    TS.select)
    , ((0, xK_Up),       TS.movePrev)
    , ((0, xK_Down),     TS.moveNext)
    , ((0, xK_Left),     TS.moveParent)
    , ((0, xK_Right),    TS.moveChild)
    , ((0, xK_k),        TS.movePrev)
    , ((0, xK_j),        TS.moveNext)
    , ((0, xK_h),        TS.moveParent)
    , ((0, xK_l),        TS.moveChild)
    , ((0, xK_o),        TS.moveHistBack)
    , ((0, xK_i),        TS.moveHistForward)
    ]
-------------------------------------------------------------------------
-- MAIN
-------------------------------------------------------------------------
main :: IO ()
main = do
    xmproc0 <- spawnPipe "xmobar -x 0 /home/sten/.config/xmobar/xmobarrc-0"
    xmproc1 <- spawnPipe "xmobar -x 1 /home/sten/.config/xmobar/xmobarrc-1"
    xmproc2 <- spawnPipe "xmobar -x 2 /home/sten/.config/xmobar/xmobarrc-2"
    xmonad $ docks def
        { terminal           = myTerminal
        , focusFollowsMouse  = myFocusFollowsMouse
        , clickJustFocuses   = myClickJustFocuses
        , borderWidth        = myBorderWidth
        , modMask            = myModMask
        , workspaces         = myWorkspaces
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
--      , keys               = myKeys
--      , mouseBindings      = myMouseBindings
        , startupHook        = myStartupHook
        , layoutHook         = myLayoutHook
--      , layoutHook         = avoidStruts  $  layoutHook defaultConfig
        , manageHook         = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageHook def
        , logHook            = dynamicLogWithPP xmobarPP
            { ppOutput       = \x -> hPutStrLn xmproc0 x >> hPutStrLn xmproc1 x >> hPutStrLn xmproc2 x
            , ppCurrent      = xmobarColor "#B8CC52" "" . wrap "[" "]"
            , ppVisible      = xmobarColor "#36A3D9" "" . wrap "" ""
            , ppHidden       = xmobarColor "#465764" "" . wrap "*" "*"
            , ppSep          = " "
            , ppTitle        = xmobarColor "#EAEAEA" "" . shorten 40
            , ppLayout       = xmobarColor "#36A3D9" ""
            , ppExtras       = [windowCount]
            , ppOrder        = \(ws:l:t:ex) -> [ws]++ex++[t]-- altro possibile [ws,l]++ex++[t] -- ws= workspace, l=layout, t=title, ex=num windows
            }
        } `additionalKeysP` myKeys

