  -- Base

import XMonad
import System.IO
import System.Exit
import qualified XMonad.StackSet as W

  -- Actions
import XMonad.Actions.SpawnOn (spawnOn, manageSpawn) -- to spawn application on specific windows

  -- Data
import qualified Data.Map as M
import Data.Monoid

  -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..)) -- see XMonad.Hooks.DynamycLog manual to setup and manage output infos for statusbar
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks 
import XMonad.Hooks.SetWMName

  --  Layout
import XMonad.Layout.IndependentScreens

  -- Utilities
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig

  -- Variables
myFont               :: String
myFont               = "xft:Mononoki Nerd Font:bold:size=9:antialias=true:hinting=true"

myTerminal           :: String
myTerminal           = "alacritty"

myModMask            :: KeyMask
myModMask            = mod4Mask     -- Sets modkey to super/windows key

myBorderWidth        :: Dimension
myBorderWidth        = 8 -- Width of the window border in pixels

myNormalBorderColor  :: String
myNormalBorderColor  = "bebebe"

myFocusedBorderColor :: String
myFocusedBorderColor = "ff0000"

myFocusFollowsMouse  :: Bool
myFocusFollowsMouse  = False -- Whether focus follows the mouse pointer.

myClickJustFocuses   :: Bool
myClickJustFocuses   = True -- Whether clicking on a window to focus also passes the click to the window

myWorkspaces         = ["1-FM","2-MAIN","3-WEB","4-MAIL","5","6","7","8","9-SYS"]

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset


  -- Autostart

myStartupHooks :: X ()
myStartupHooks = do
  spawnOnce "lxsession &"
  spawnOnce "/usr/bin/stalonetray &"
  spawnOnce "picom --config $HOME/.config/picom/picom.conf &"
  spawnOnce "synology-drive autostart &"
  spawnOnce "/opt/protonmail-bridge/proton-bridge --no-window &"
  spawnOnce "feh --bg-scale ~/Pictures/Wallpapers/arch02.jpg &"
  spawnOnce "flameshot &"
  spawnOnce "nm-applet &"
  spawnOnce "blueman-applet &"
  spawnOnce "pulseaudio -D &"
  spawnOnce "pa-applet &"
  spawnOnce "/usr/bon/emacs --daemon &"
  setWMName "LG3D" -- May be useful for making Java GUI programs work



------------------------------------------------------------------------

------------------------------------------------------------------------
-- KEY BINDINGS



-- MOUSE BINDINGS


------------------------------------------------------------------------
-- HOOKS, LAYOUTS

  -- LogHook
{- XMonad.Hooks.DynamicLog
xmonad calls the logHook with every internal state update, which is useful for (among other things) outputting status information to an external status bar program such as xmobar or dzen. DynamicLog provides several drop-in logHooks for this purpose, as well as flexible tools for specifying your own formatting.
-} 

myManageHooks = composeAll
  [ [ className =? "Firefox-bin"    --> doShift "3-WEB" ]
  , [ className =? "Evolution"      --> doShift "4-MAIL" ]
--  , [ classname =? "thunar"         --> doShift "1-FM" ]
--  , [ classname =? "proton-bridge"  --> doFloat ]
--  , manageDocks
  ]


  
  -- Layouts
  
myLayoutHooks = avoidStruts (tiled ||| Mirror tiled ||| Full)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio
     -- The default number of windows in the master pane
     nmaster = 1
     -- Default proportion of screen occupied by master pane
     ratio   = 1/2
     -- Percent of screen to increment by when resizing panes
     delta   = 3/100


  -- General settings

conf = ewmh defaultConfig
    { manageHook = myManageHooks <+> manageDocks <+> manageHook defaultConfig
    , layoutHook = myLayoutHooks
    , logHook = dynamicLogWithPP xmobarPP
                         { ppOutput = hPutStrLn xmproc
                         , ppCurrent = xmobarColor "#98be65" "" . wrap "[" "]" -- Current workspace in xmobar
                         , ppVisible = xmobarColor "#98be65" ""                -- Visible but not current workspace
                         , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                         , ppHiddenNoWindows = xmobarColor "#c792ea" ""        -- Hidden workspaces (no windows)
                         , ppTitle = xmobarColor "#b3afc2" "" . shorten 60     -- Title of active window in xmobar
                         , ppSep =  "<fc=#666666> <fn=2>|</fn> </fc>"          -- Separators in xmobar
--                         , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                         , ppExtras  = [windowCount]                           -- # of windows current workspace
                         , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                         }
    }


  -- xmobar
{-  
myXmobarPP0 h = xmobarPP
      { ppOutput = hPutStrLn h
      , ppCurrent = xmobarColor "#B8CC52" "" . wrap "[" "]"
      , ppVisible = xmobarColor "#36A3D9" "" . wrap "" ""
      , ppHidden = xmobarColor "#465764" "" . wrap "*" "*"
      , ppSep = " "
      , ppTitle = xmobarColor "#EAEAEA" "" . shorten 35
      , ppLayout = xmobarColor "#36A3D9" ""
      , ppExtras  = [windowCount]
      , ppOrder  = \(ws:l:t:ex) -> [ws]++ex --alternative [ws,l]++ex++[t] -- ws= workspace, l=layout, t=title of current window, ex=num of windows
      }

myXmobarPP1 h = xmobarPP
      { ppOutput = hPutStrLn h
      , ppCurrent = xmobarColor "#B8CC52" "" . wrap "[" "]"
      , ppVisible = xmobarColor "#36A3D9" "" . wrap "" ""
      , ppHidden = xmobarColor "#465764" "" . wrap "*" "*"
      , ppSep = " "
      , ppTitle = xmobarColor "#EAEAEA" "" . shorten 35
      , ppLayout = xmobarColor "#36A3D9" ""
      , ppExtras  = [windowCount]
      , ppOrder  = \(ws:l:t:ex) -> [ws]++ex --alternative [ws,l]++ex++[t] -- ws= workspace, l=layout, t=title of current window, ex=num of windows
      }
-}

  -- Main
main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar -x 0 /home/sten/.config/xmobar/xmobarrc-0"
--  xmproc1 <- spawnPipe "xmobar -x 1 /home/sten/.config/xmobar/xmobarrc-0"
--  xmproc2 <- spawnPipe "xmobar -x 2 /home/sten/.config/xmobar/xmobarrc-0"
  xmonad conf
      { terminal           = myTerminal
      , focusFollowsMouse  = myFocusFollowsMouse
      , clickJustFocuses   = myClickJustFocuses
      , borderWidth        = myBorderWidth
      , modMask            = myModMask
      , workspaces         = myWorkspaces
      , normalBorderColor  = myNormalBorderColor
      , focusedBorderColor = myFocusedBorderColor
--    , keys               = myKeys
--    , mouseBindings      = myMouseBindings
      , startupHook        = startupHook conf >> setWMName "LG3D"
      }
